/*global require, console*/
/*eslint no-console: 0 */

require.config({
  paths: {
    leaflet: "./../bower_components/leaflet/dist/leaflet",
    handlebars: "./../bower_components/handlebars/handlebars.min",
    jquery: "./../bower_components/jquery/dist/jquery.min"
  }
});


require(["leaflet", "handlebars", "jquery"], function(L, handlebars, $) {
  "use strict";

  // create a map in the "map" div, set the view to a given place and zoom
  var map = L.map("map");
  var bounds = [
    [54.471093, 6.062017],
    [47.310265, 14.389654]
  ];
  map.fitBounds(bounds);

  // add an OpenStreetMap tile layer
  L.tileLayer("http://{s}.tile.osm.org/{z}/{x}/{y}.png", {
    attribution: "&copy; <a href='http://osm.org/copyright'>OpenStreetMap</a> contributors"
  }).addTo(map);


  function addLayerToMap(url, fillColor, color){
    $.get(url).done(function(data) {
        L.geoJson(data, {
          style: {
            fillColor: fillColor,
            color: color
          }
        }).addTo(map);
      })
      .fail(function(jqXHR, textStatus, errorThrown) {
        console.error(errorThrown);
      });
  }

  addLayerToMap("http://localhost:3000/before", "#d0d0d0", "#0000cc");
  addLayerToMap("http://localhost:3000/changed", "#d8c92d", "#ff0000");

});
